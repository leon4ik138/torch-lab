import torch
import numpy as np
import os
import logging
from utils import const_variables

from bert.bert_train import BinaryClassificationProcessor
from sklearn.metrics import matthews_corrcoef, confusion_matrix
from torch.utils.data import (DataLoader, SequentialSampler, TensorDataset)
from torch.nn import CrossEntropyLoss, MSELoss
from multiprocessing import Pool, cpu_count
from utils.convert_examples_to_features import convert_example_to_feature
from tqdm import tqdm_notebook
from pytorch_pretrained_bert import BertTokenizer, BertForSequenceClassification

logging.basicConfig(level=logging.INFO)
logger = logging.Logger(name = 'logger')

os.makedirs(const_variables.REPORTS_DIR, exist_ok=True)


def get_eval_report(task_name, labels, preds):
    mcc = matthews_corrcoef(labels, preds)
    tn, fp, fn, tp = confusion_matrix(labels, preds).ravel()
    return {
        "task": task_name,
        "mcc": mcc,
        "tp": tp,
        "tn": tn,
        "fp": fp,
        "fn": fn
    }


def compute_metrics(task_name, labels, preds):
    assert len(preds) == len(labels)
    return get_eval_report(task_name, labels, preds)


tokenizer = BertTokenizer.from_pretrained(const_variables.OUTPUT_DIR, do_lower_case=True)
processor = BinaryClassificationProcessor()
eval_examples = processor.get_dev_examples(const_variables.DATA_DIR)
label_list = processor.get_labels()    # [0, 1] for binary classification
num_labels = len(label_list)
eval_examples_len = len(eval_examples)
label_map = {label: i for i, label in enumerate(label_list)}
eval_examples_for_processing = [(example, label_map, const_variables.MAX_SEQ_LENGTH, tokenizer, const_variables.OUTPUT_MODE) for example in eval_examples]
process_count = cpu_count() - 1
if __name__ ==  '__main__':
    print('Preparing to convert ', eval_examples_len, ' examples..')
    print('Spawning', process_count, ' processes..')
    with Pool(process_count) as p:
        eval_features = list(tqdm_notebook(p.imap(convert_example_to_feature, eval_examples_for_processing), total=eval_examples_len))
all_input_ids = torch.tensor([f.input_ids for f in eval_features], dtype=torch.long)
all_input_mask = torch.tensor([f.input_mask for f in eval_features], dtype=torch.long)
all_segment_ids = torch.tensor([f.segment_ids for f in eval_features], dtype=torch.long)
if const_variables.OUTPUT_MODE == "classification":
    all_label_ids = torch.tensor([f.label_id for f in eval_features], dtype=torch.long)
elif const_variables.OUTPUT_MODE == "regression":
    all_label_ids = torch.tensor([f.label_id for f in eval_features], dtype=torch.float)


eval_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids, all_label_ids)
device = const_variables.device
# Run prediction for full data
eval_sampler = SequentialSampler(eval_data)
eval_dataloader = DataLoader(eval_data, sampler=eval_sampler, batch_size=const_variables.EVAL_BATCH_SIZE)
model = BertForSequenceClassification.from_pretrained(const_variables.OUTPUT_DIR, cache_dir=const_variables.CACHE_DIR, num_labels=len(label_list))
model.to(device)
model.eval()
eval_loss = 0
nb_eval_steps = 0
preds = []

for input_ids, input_mask, segment_ids, label_ids in tqdm_notebook(eval_dataloader, desc="Evaluating"):
    input_ids = input_ids.to(device)
    input_mask = input_mask.to(device)
    segment_ids = segment_ids.to(device)
    label_ids = label_ids.to(device)

    with torch.no_grad():
        logits = model(input_ids, segment_ids, input_mask, labels=None)

    # create eval loss and other metric required by the task
    if const_variables.OUTPUT_MODE == "classification":
        loss_fct = CrossEntropyLoss()
        tmp_eval_loss = loss_fct(logits.view(-1, num_labels), label_ids.view(-1))
    elif const_variables.OUTPUT_MODE == "regression":
        loss_fct = MSELoss()
        tmp_eval_loss = loss_fct(logits.view(-1), label_ids.view(-1))

    eval_loss += tmp_eval_loss.mean().item()
    nb_eval_steps += 1
    if len(preds) == 0:
        preds.append(logits.detach().cpu().numpy())
    else:
        preds[0] = np.append(
            preds[0], logits.detach().cpu().numpy(), axis=0)

eval_loss = eval_loss / nb_eval_steps
preds = preds[0]
if const_variables.OUTPUT_MODE == "classification":
    preds = np.argmax(preds, axis=1)
elif const_variables.OUTPUT_MODE == "regression":
    preds = np.squeeze(preds)
result = compute_metrics(const_variables.TASK_NAME, all_label_ids.numpy(), preds)

result['eval_loss'] = eval_loss

output_eval_file = os.path.join(const_variables.REPORTS_DIR, "eval_results.txt")
with open(output_eval_file, "w") as writer:
    logger.info("***** Eval results *****")
    for key in (result.keys()):
        logger.info("  %s = %s", key, str(result[key]))
        writer.write("%s = %s\n" % (key, str(result[key])))

