from __future__ import absolute_import, division, print_function
import csv
import logging
import torch
import pickle
import os

from utils import const_variables
from utils.data_processor import BinaryClassificationProcessor
from utils.convert_examples_to_features import convert_example_to_feature
from utils.text_corrector import TextCorrector

from torch.utils.data import (DataLoader, RandomSampler, TensorDataset)
from torch.nn import CrossEntropyLoss, MSELoss

from tqdm import tqdm_notebook, trange
from pytorch_pretrained_bert import BertTokenizer, BertForSequenceClassification
from pytorch_pretrained_bert.optimization import BertAdam

from multiprocessing import Pool, cpu_count

text_corrector = TextCorrector()
logger = logging.getLogger()
csv.field_size_limit(2147483647) # Increase CSV reader's field limit incase we have long text.


# OPTIONAL: if you want to have more information on what's happening, activate the logger as follows
logging.basicConfig(level=logging.INFO)

device = const_variables.device
output_mode = const_variables.OUTPUT_MODE
cache_dir = const_variables.CACHE_DIR

os.makedirs(const_variables.REPORTS_DIR, exist_ok=True)


processor = BinaryClassificationProcessor()
train_examples = processor.get_train_examples(const_variables.DATA_DIR)
train_examples_len = len(train_examples)
label_list = processor.get_labels() # [0, 1] for binary classification
num_labels = len(label_list)
num_train_optimization_steps = int(
    train_examples_len / const_variables.TRAIN_BATCH_SIZE / const_variables.GRADIENT_ACCUMULATION_STEPS) * const_variables.NUM_TRAIN_EPOCHS

tokenizer = BertTokenizer.from_pretrained(const_variables.BERT_MODEL, do_lower_case=True)
label_map = {label: i for i, label in enumerate(label_list)}
train_examples_for_processing = [(example, label_map, const_variables.MAX_SEQ_LENGTH, tokenizer, const_variables.OUTPUT_MODE) for example in train_examples]
process_count = cpu_count() - 1


if __name__ ==  '__main__':
    print('Preparing to convert ', train_examples_len, ' examples..')
    print('Spawning ', {process_count}, 'processes..')
    with Pool(process_count) as p:
        train_features = list(tqdm_notebook(p.imap(convert_example_to_feature, train_examples_for_processing), total=train_examples_len))
    with open("train_features.pkl", "wb") as f:
        pickle.dump(train_features, f)
    model = BertForSequenceClassification.from_pretrained(const_variables.BERT_MODEL, cache_dir=const_variables.CACHE_DIR, num_labels=num_labels)
    model.to(device)
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 0.1},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
    ]
    optimizer = BertAdam(optimizer_grouped_parameters,
                         lr=const_variables.LEARNING_RATE,
                         warmup=-1,
                         t_total=num_train_optimization_steps)

    global_step = 0
    nb_tr_steps = 0
    tr_loss = 0
    logger.info("***** Running training *****")
    logger.info("  Num examples = %d", train_examples_len)
    logger.info("  Batch size = %d", const_variables.TRAIN_BATCH_SIZE)
    logger.info("  Num steps = %d", num_train_optimization_steps)
    all_input_ids = torch.tensor([f.input_ids for f in train_features], dtype=torch.long)
    all_input_mask = torch.tensor([f.input_mask for f in train_features], dtype=torch.long)
    all_segment_ids = torch.tensor([f.segment_ids for f in train_features], dtype=torch.long)

    if const_variables.OUTPUT_MODE == "classification":
        all_label_ids = torch.tensor([f.label_id for f in train_features], dtype=torch.long)
    elif const_variables.OUTPUT_MODE == "regression":
        all_label_ids = torch.tensor([f.label_id for f in train_features], dtype=torch.float)
    train_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids, all_label_ids)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=const_variables.TRAIN_BATCH_SIZE)
    model.train()
    for _ in trange(int(const_variables.NUM_TRAIN_EPOCHS), desc="Epoch"):
        tr_loss = 0
        nb_tr_examples, nb_tr_steps = 0, 0
        for step, batch in enumerate(tqdm_notebook(train_dataloader, desc="Iteration")):
            batch = tuple(t.to(device) for t in batch)
            input_ids, input_mask, segment_ids, label_ids = batch

            logits = model(input_ids, segment_ids, input_mask, labels=None)

            if const_variables.OUTPUT_MODE == "classification":
                loss_fct = CrossEntropyLoss()
                loss = loss_fct(logits.view(-1, num_labels), label_ids.view(-1))
            elif const_variables.OUTPUT_MODE == "regression":
                loss_fct = MSELoss()
                loss = loss_fct(logits.view(-1), label_ids.view(-1))

            if const_variables.GRADIENT_ACCUMULATION_STEPS > 1:
                loss = loss / const_variables.GRADIENT_ACCUMULATION_STEPS

            loss.backward()
            print("\r%f" % loss, end='')

            tr_loss += loss.item()
            nb_tr_examples += input_ids.size(0)
            nb_tr_steps += 1
            if (step + 1) % const_variables.GRADIENT_ACCUMULATION_STEPS == 0:
                optimizer.step()
                optimizer.zero_grad()
                global_step += 1
    model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model it-self

    output_model_file = os.path.join(const_variables.OUTPUT_DIR, const_variables.WEIGHTS_NAME)
    output_config_file = os.path.join(const_variables.OUTPUT_DIR, const_variables.CONFIG_NAME)

    torch.save(model_to_save.state_dict(), output_model_file)
    model_to_save.config.to_json_file(output_config_file)
    tokenizer.save_vocabulary(const_variables.OUTPUT_DIR)



