import argparse
import torch

from utils import const_variables
from pytorch_pretrained_bert import BertTokenizer, BertForSequenceClassification
from utils.convert_examples_to_features import convert_text_to_feature
from utils.text_corrector import TextCorrector


def predict(text, model, tokenizer, text_corrector):
    text = text_corrector.correct(text)
    input_features = convert_text_to_feature([text, const_variables.label_map, const_variables.MAX_SEQ_LENGTH, tokenizer, const_variables.OUTPUT_MODE])
    input_ids = input_features.input_ids
    segment_ids = input_features.segment_ids
    input_mask = input_features.input_mask
    input_ids = torch.tensor(input_ids).to(const_variables.device).unsqueeze(0)
    input_mask = torch.tensor(input_mask).to(const_variables.device).unsqueeze(0)
    segment_ids = torch.tensor(segment_ids).to(const_variables.device).unsqueeze(0)
    with torch.no_grad():
        logits = model(input_ids, segment_ids, input_mask, labels=None)
    torch.nn.Softmax(logits)
    return logits[0][1]


if __name__ == '__main__':
    corrector = TextCorrector()
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, help='absolute or relative path to bert "output/" directory', default='outputs/yelp')
    args = parser.parse_args()
    model_path = args.model_path
    model = BertForSequenceClassification.from_pretrained(model_path, cache_dir=const_variables.CACHE_DIR, num_labels=len(
        const_variables.label_map))
    model.cuda()
    tokenizer = BertTokenizer.from_pretrained(model_path, do_lower_case=True)
    print('Input text to classify it. To stop, write EXIT')
    text = input()
    while text != 'EXIT':
        logit = predict(text, model, tokenizer, corrector)
        print('Probability of this text to be a Disaster tweet is ', torch.nn.Softmax(logit).dim.item())
        text = input()
