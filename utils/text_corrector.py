import re


class TextCorrector:

    def correct(self, text):
        text = re.sub('\u200b', "", text)                                                       # this handles Unicode Character 'ZERO WIDTH SPACE' (U+200B)
        text = re.sub('\n', " ", text)
        text = re.sub('\!|\@|\$|\*|\[|\]|\(|\)|\"|\^|\~|\{|\}|\<|\>|\,|\.|\'|\:', ' ', text)    # removes  symbols:     !@$*[]()"^~{}<>,.:'
        text = re.sub('\№', ' number ', text)                                                   # replaces symbols:    №    to:  "number"
        text = re.sub('\&', ' and ', text)                                                      # replaces symbols:     &     to:  "and"
        text = re.sub('\%', ' percent ', text)                                                  # replaces symbols:     %     to:  "percent"
        text = re.sub(r'\s+/\s+', ' or ', text)                                                 # replaces symbols:     bathe / sink   to:  bathe or sink
        word_list = re.findall(r"[\w']+", text)
        word_numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six',
                        'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve',
                        'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen',
                        'eighteen', 'nineteen', 'twenty', 'thirty', 'forty',
                        'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred',
                        'thousand', 'million', 'milliard', 'billion']   
        for n, word_to_be_replaced in enumerate(word_list):
            if word_to_be_replaced in word_numbers:
                word_list[n] = '➀➀➀➀➀'
        text = " ".join(word_list)
        text = re.sub(r"\d+", ' ➀➀➀➀➀ ', text)
        text = text.lower()
        text = re.sub('\s+',' ',text)                                                           # replaces multiple spaces
        return text

        
if __name__ == '__main__':
    textCorrector = TextCorrector()
    text = "first name is the one #1 in the word of 2000 cats"
    print(textCorrector.correct(text))